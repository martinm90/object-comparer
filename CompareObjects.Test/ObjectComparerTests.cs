﻿using CompareObjects;
using CompareObjects.TestModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace CompareObjectsTests
{
    [TestClass]
    public class ObjectComparerTests
    {
        private IObjectComparer _target;
        private IObjectDifferenceChecker _objectDifferenceChecker;
        private IChangeMessageHandler _changeMessageHandler;

        public ObjectComparerTests()
        {
            _objectDifferenceChecker = new ObjectDifferenceChecker();
            _changeMessageHandler = new ChangeMessageHandler();
            _target = new ObjectComparer(_objectDifferenceChecker, _changeMessageHandler);
        }

        [TestMethod]
        public void TwoObjectsOfTypePersonPropertiesExpectDifferences()
        {
            var personOne = new PersonProperties("David", new DateTime(1990, 5, 2), 23);
            var personTwo = new PersonProperties("Janet", new DateTime(2000, 11, 22), 23);

            var result = _target.GetChanges(personOne, personTwo);

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("Forename changed from 'David' to 'Janet'", result[0]);
            Assert.AreEqual("Date Of Birth changed from '02/May/1990' to '22/Nov/2000'", result[1]);
        }

        [TestMethod]
        public void TwoObjectsOfTypePersonFieldsExpectDifferences()
        {
            //This test is based an extra/assumption that it may be required to check objects
            //with public fields, as well as properties.

            var personOne = new PersonFields("David", new DateTime(1990, 5, 2), 23);
            var personTwo = new PersonFields("Janet", new DateTime(2000, 11, 22), 23);

            var result = _target.GetChanges(personOne, personTwo);

            Assert.AreEqual(2, result.Count);
            Assert.AreEqual("Forename changed from 'David' to 'Janet'", result[0]);

            // Display name is not possible on fields, so the text "DateOfBirth" is used
            Assert.AreEqual("DateOfBirth changed from '02/May/1990' to '22/Nov/2000'", result[1]);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void TwoObjectsOfDifferentTypeExpectException()
        {
            var personOne = new PersonProperties("David", new DateTime(1990, 5, 2), 23);
            var personTwo = new PersonFields("Janet", new DateTime(2000, 11, 22), 23);

            var result = _target.GetChanges(personOne, personTwo);
        }
    }


}
