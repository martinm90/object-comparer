﻿using CompareObjects.MessageFormatting;
using CompareObjects.Models;
using System.Collections.Generic;

namespace CompareObjects
{
    public interface IChangeMessageHandler
    {
        IList<string> GetMessages(IEnumerable<Difference> differences);
    }

    public class ChangeMessageHandler : IChangeMessageHandler
    {
        public IList<string> GetMessages(IEnumerable<Difference> differences)
        {
            var messages = new List<string>();
            foreach (var difference in differences)
            {
                string originalValue = GetFormattedValue(difference.OriginalValue);
                string changedValue = GetFormattedValue(difference.ChangedValue);

                string message = string.Format("{0} changed from '{1}' to '{2}'",
                                    difference.PropertyName, originalValue, changedValue);
                messages.Add(message);
            }

            return messages;
        }

        private string GetFormattedValue(object objectValue)
        {
            // A factory is used to create message string formatters here.
            // this makes its easier to extend and add functionality
            // for other types in the future.

            var messageFormatter = MessageFormatterFactory.GetMessage(objectValue);
            return messageFormatter.GetFormattedMessage(objectValue);
        }
    }
}
