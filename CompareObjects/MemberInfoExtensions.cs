﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Linq;

namespace CompareObjects
{
    public static class MemberInfoExtensions
    {
        public static object GetValue(this MemberInfo memberInfo, object objectToCompare)
        {
            if (memberInfo.MemberType == MemberTypes.Property)
            {
                return objectToCompare.GetType().GetProperty(memberInfo.Name).GetValue(objectToCompare);
            }
            else if (memberInfo.MemberType == MemberTypes.Field)
            {
                return objectToCompare.GetType().GetField(memberInfo.Name).GetValue(objectToCompare);
            }

            throw new ApplicationException("Value is not a public field or property.");
        }

        public static string GetMemberName(this MemberInfo memberInfo)
        {
            var displayName = memberInfo.GetCustomAttribute<DisplayNameAttribute>();

            if (displayName == null)
            {
                return memberInfo.Name;
            }
            else
            {
                return displayName.DisplayName;
            }
        }
    }
}
