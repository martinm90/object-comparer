﻿using System;

namespace CompareObjects.MessageFormatting
{
    public class DateTimeChangeMessageFormatter : IChangeMessageFormatter
    {
        public string GetFormattedMessage(object objectValue)
        {
            var date = (DateTime)objectValue;
            return date.Date.ToString("dd/MMM/yyyy");
        }
    }
}
