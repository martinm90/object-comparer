﻿
namespace CompareObjects.MessageFormatting
{
    public class DefaultChangeMessageFormatter : IChangeMessageFormatter
    {
        public string GetFormattedMessage(object objectValue)
        {
            return objectValue.ToString();
        }
    }
}
