﻿
namespace CompareObjects.MessageFormatting
{
    public interface IChangeMessageFormatter
    {
        string GetFormattedMessage(object objectValue);
    }
}
