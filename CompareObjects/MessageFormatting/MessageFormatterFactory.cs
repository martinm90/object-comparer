﻿using System;

namespace CompareObjects.MessageFormatting
{
    public static class MessageFormatterFactory
    {
        public static IChangeMessageFormatter GetMessage(object objectValue)
        {
            if (objectValue is DateTime)
            {
                return new DateTimeChangeMessageFormatter();
            }
            else
            {
                return new DefaultChangeMessageFormatter();
            }
        }
    }
}
