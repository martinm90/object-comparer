﻿
namespace CompareObjects.Models
{
    public class Difference
    {
        public string PropertyName { get; set; }
        public object OriginalValue { get; set; }
        public object ChangedValue { get; set; }
    }
}
