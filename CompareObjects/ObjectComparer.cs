﻿using System;
using System.Collections.Generic;

namespace CompareObjects
{
    public interface IObjectComparer
    {
        IList<string> GetChanges(object originalObject, object changedObject);
    }

    public class ObjectComparer : IObjectComparer
    {
        private IObjectDifferenceChecker _objectDifferenceChecker;
        private IChangeMessageHandler _changeMessageHandler;

        public ObjectComparer(IObjectDifferenceChecker objectDifferenceChecker, IChangeMessageHandler changeMessageHandler)
        {
            _objectDifferenceChecker = objectDifferenceChecker;
            _changeMessageHandler = changeMessageHandler;
        }

        public IList<string> GetChanges(object originalObject, object changedObject)
        {
            CheckObjectTypesMatch(originalObject, changedObject);
            var differences = _objectDifferenceChecker.GetDifferences(originalObject, changedObject);

            return _changeMessageHandler.GetMessages(differences);
        }

        private void CheckObjectTypesMatch(object originalObject, object changedObject)
        {
            if (originalObject.GetType() != changedObject.GetType())
            {
                throw new ApplicationException("Objects must be of the same type.");
            }
        }
    }
}
