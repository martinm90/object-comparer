﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompareObjects
{
    public static class ObjectComparerTypeRegister
    {
        public static void RegisterTypes(UnityContainer container)
        {
            container.RegisterType<IChangeMessageHandler, ChangeMessageHandler>();
            container.RegisterType<IObjectComparer, ObjectComparer>();
            container.RegisterType<IObjectDifferenceChecker, ObjectDifferenceChecker>();
        }
    }
}
