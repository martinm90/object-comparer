﻿using CompareObjects.Models;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CompareObjects
{
    public interface IObjectDifferenceChecker
    {
        IList<Difference> GetDifferences(object originalObject, object changedObject);
    }

    public class ObjectDifferenceChecker : IObjectDifferenceChecker
    {
        public IList<Difference> GetDifferences(object originalObject, object changedObject)
        {
            List<Difference> differences = new List<Difference>();
            IList<MemberInfo> members = GetMembers(originalObject);

            foreach (MemberInfo member in members)
            {
                var difference = new Difference
                {
                    PropertyName = member.GetMemberName(),
                    OriginalValue = member.GetValue(originalObject),
                    ChangedValue = member.GetValue(changedObject)
                };

                if (IsChanged(difference))
                {
                    differences.Add(difference);
                }

            }
            return differences;
        }

        private static bool IsChanged(Difference difference)
        {
            return !difference.OriginalValue.Equals(difference.ChangedValue);
        }

        private static IList<MemberInfo> GetMembers(object objectOne)
        {
            const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.Instance;

            return objectOne.GetType().GetFields(bindingFlags).Cast<MemberInfo>()
                .Concat(objectOne.GetType().GetProperties(bindingFlags)).ToList();
        }
    }
}
