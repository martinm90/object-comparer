﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CompareObjects;

namespace CompareObjects.Tests
{
    [TestClass]
    public class ObjectComparerTest
    {
        private IObjectComparer _target;

        public ObjectComparerTest()
        {
            _target = new ObjectComparer();
        }

        [TestMethod]
        public void TwoObjectsOfTypePersonExpectDifferences()
        {
            var personOne = new Person
            {
                Forename = "David",
                DateOfBirth = new DateTime(1990, 5, 2),
                SpecialNumber = 23
            };

            var personTwo = new Person
            {
                Forename = "Janet",
                DateOfBirth = new DateTime(2000, 11, 22),
                SpecialNumber = 23
            };



        }
    }

    public class Person
    {
        public string Forename { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int SpecialNumber { get; set; }
    }
}
