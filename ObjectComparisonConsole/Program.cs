﻿using CompareObjects;
using CompareObjects.TestModels;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectComparisonConsole
{
    class Program
    {
        private static IObjectComparer _objectComparer;

        static void Main(string[] args)
        {
            _objectComparer = UnityRegister.RegisterTypes(new UnityContainer()).Resolve<IObjectComparer>();

            Console.WriteLine("Comparing 'Person' objects, with the values:");
            Console.WriteLine("--------------");
            Console.WriteLine("Person One");
            var personOne = new PersonProperties("Dave", new DateTime(1990, 5, 2), 23);
            Console.WriteLine(personOne);
            Console.WriteLine("--------------");
            Console.WriteLine("Person Two");
            var personTwo = new PersonProperties("Janet", new DateTime(2000, 11, 22), 23);
            Console.WriteLine(personTwo);
            Console.WriteLine("--------------");
            Console.WriteLine("Output");

            var differences = _objectComparer.GetChanges(personOne, personTwo);

            foreach(string difference in differences)
            {
                Console.WriteLine(difference);
            }

            Console.ReadKey();
        }
    }
}
