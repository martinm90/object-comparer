﻿using CompareObjects;
using Microsoft.Practices.Unity;

namespace ObjectComparisonConsole
{
    public class UnityRegister
    {
        public static UnityContainer RegisterTypes(UnityContainer container)
        {
            ObjectComparerTypeRegister.RegisterTypes(container);
            return container;
        }
    }
}
