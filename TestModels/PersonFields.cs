﻿using System;
using System.ComponentModel;

namespace CompareObjects.TestModels
{
    public class PersonFields
    {
        public PersonFields(string forename, DateTime dateOfBirth, int specialNumber)
        {
            this.Forename = forename;
            this.DateOfBirth = dateOfBirth.Date;
            this.SpecialNumber = specialNumber;
        }

        public string Forename;

        public DateTime DateOfBirth;
        
        public int SpecialNumber;
    }
}
