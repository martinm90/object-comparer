﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace CompareObjects.TestModels
{
    public class PersonProperties
    {
        public PersonProperties(string forename, DateTime dateOfBirth, int specialNumber)
        {
            this.Forename = forename;
            this.DateOfBirth = dateOfBirth.Date;
            this.SpecialNumber = specialNumber;
        }

        public string Forename { get; set; }

        [DisplayName("Date Of Birth")]
        public DateTime DateOfBirth { get; set; }

        public int SpecialNumber { get; set; }

        public override string ToString()
        {
            return "Forename: " + this.Forename +
                    Environment.NewLine +
                    "Date Of Birth: " + DateOfBirth.Date +
                    Environment.NewLine +
                    "Special Number: " + SpecialNumber;
        }
    }
}
